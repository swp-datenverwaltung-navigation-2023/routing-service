package de.fuberlin.navigator.routingserver.model;

import java.util.Arrays;
import java.util.Objects;

public enum WeatherTag {
    RAIN("rain"), SNOW("snow"), ICE("ice"), WIND("wind");

    private String code;

    WeatherTag(String code) {
        this.code = code;
    }

    public static WeatherTag findByCode(String code) {
        for (WeatherTag tag : values()) {
            if (tag.code.equals(code)) {
                return tag;
            }
        }

        return null;
    }
}
