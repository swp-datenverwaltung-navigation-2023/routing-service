package de.fuberlin.navigator.routingserver.serialization;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import de.fuberlin.navigator.routingserver.exception.RequestBodyFormatException;
import de.fuberlin.navigator.routingserver.model.Point;
import de.fuberlin.navigator.routingserver.model.RoutingRequest;
import de.fuberlin.navigator.routingserver.model.WeatherTag;

public class RoutingRequestDeserializer extends StdDeserializer<RoutingRequest> {

    public RoutingRequestDeserializer() {
        this(null);
    }

    public RoutingRequestDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public RoutingRequest deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException, JacksonException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        Point startNode = new Point(
                node.get("start_node").get("lat").asDouble(),
                node.get("start_node").get("lon").asDouble());
        Point endNode = new Point(
                node.get("end_node").get("lat").asDouble(),
                node.get("end_node").get("lon").asDouble());

        Set<WeatherTag> forbidden = new HashSet<>();
        JsonNode forbiddenTags = node.get("forbidden");
        if (!forbiddenTags.isArray()) {
            throw new RequestBodyFormatException("The structure of the \"forbidden\" field must be a list.");
        }
        for (JsonNode arrNode : forbiddenTags) {
            forbidden.add(WeatherTag.findByCode(arrNode.textValue()));
        }

        int startAfterToday = node.get("start_after_today").asInt();
        int endAfterToday = node.get("end_after_today").asInt();

        return new RoutingRequest(startNode, endNode, forbidden, startAfterToday, endAfterToday);
    }
}