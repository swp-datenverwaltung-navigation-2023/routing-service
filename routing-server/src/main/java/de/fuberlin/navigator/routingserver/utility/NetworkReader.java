package de.fuberlin.navigator.routingserver.utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import de.fuberlin.navigator.protos.map_builder.RoadNetwork;
import de.fuberlin.navigator.protos.map_builder.RoadNetwork.Builder;



public class NetworkReader{
    public static void main(String[] args) throws IOException {
        // testing
        System.out.println(readRoadNetwork());

    }

    // read from network file and print content
    public static RoadNetwork readRoadNetwork(){
        Builder builder = RoadNetwork.newBuilder();
            
        try {
            FileInputStream input = new FileInputStream(Config.ROAD_NETWORK_INPUT_PATH);
            RoadNetwork roadnetwork = builder.mergeFrom(input).build();
            return roadnetwork;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
            

        
    }
 }

