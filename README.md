# Routing Service

## Create Proto Classes

------

In order to create classes from the protos (in case the definition changed) one has to use the following command : 

```sh
protoc --proto_path=debug-tool/proto/schema --python_out=./debug-tool/proto debug-tool/proto/schema/roadnetwork.proto
```

## Endpoints

<ins>/route</ins> gets as input a routing request consisting of start and end node, forbidden weather conditions and the period of time the search should take place. It returns a list of coordinates of the found path, as well as the duration, distance, the day of the route and an error code.

<ins>/addressmatching</ins> converts a given address to its coordinates. It returns the coordinate pair and an error code. For the address matching we are using Nominatim API to get the OSM data. 

#### Description of steps to find the shortest path

- Nodes and Edges of our road network are added to a directed Graph

- if the street/edge is no oneway street the opposite edge is added as well

- the weight of the edges correspond to the duration of passing the edge

- the start point, which is passed on the algorithm is matched to a segment on the map 

- AStar algorithm is applied to resulting graph by using AStarShortestPath by JGraphT

- algorithm is adapted to our requirements by checking for turn restrictions and forbidden weather conditions for the edges of the path and by searching in a given period of time 

- heuristic: haversine distance between current and target node


Further details of the project can be found in our [organisational page](https://git.imp.fu-berlin.de/swp-datenverwaltung-navigation-2023/organisational), where our project report can be downloaded.
## Start the application in Docker
To start the appliaction in Docker, you have to clone the  repositories of [map-data-parser](https://git.imp.fu-berlin.de/swp-datenverwaltung-navigation-2023/map-data-parser), [metric-builder](https://git.imp.fu-berlin.de/swp-datenverwaltung-navigation-2023/metric-builder) and routing-service (the one you are on right now) into your local system. Inside the headfolders of these repositories, there are Docker Image files named <servicename>.tar. You have to load these images into your local Docker enviroment. This can be easily done by pasting  
```sh
docker load < <servicename>.tar
```
into your bash or 
```sh
docker load -i <servicename>.tar
```
into your Windows command line. After that you are ready to start the Backend application. To do this you have to run the docker-compose.yml. To do this run the following commands.
```sh
cd /your/path/to/the/routing-service-head-directory
docker-compose up
```

This will start the Docker services network on your local machine. To effectively use the app, you first have to create the needed resources like the road network or the metrics. This is done automatically by Cronjobs, but if you can't wait a day, there is also a way to do it manually. For that you need to go into the bash shell of your running containers. Depending on your installed Docker version, this can be done in different ways so check out the Docker documentation if you have questions. Once you found out, go to the bash shell of the map-builder and run the following command.
```sh
java -jar -Xmx4G app.jar
```
This command will start the map-builder function and create a road network for you. Be prepared to wait about 15 to 30 minutes though as this process can consume lots of resources. Once the process terminates, checkout if everything went smoothely. The process will write how many nodes, ways and turn restrictions were created into your console. If it shows values biggger than 0, it worked. After that you can create the metrics for the weather information. To do that, go into the bash shell of the metric-builder and run the following command.
```sh
java -jar -Xmx4G app.jar
```
This command will start the metric-builder. The process of creating the metrics will only last for a minute and if your road network was created successfully, there should not be any problem. Once the process finished, the application is ready to run. When you want to test the app locally with an emulator, you can just start it. Otherwise you maybe need to change the host adress of the server inside the flutter code. Checkout the [navigation-app](https://git.imp.fu-berlin.de/swp-datenverwaltung-navigation-2023/navigation-app) for more information.
