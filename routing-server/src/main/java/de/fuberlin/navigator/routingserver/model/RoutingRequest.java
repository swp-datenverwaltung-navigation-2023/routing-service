package de.fuberlin.navigator.routingserver.model;

import static java.util.stream.Collectors.joining;

import java.util.Set;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import de.fuberlin.navigator.routingserver.serialization.RoutingRequestDeserializer;

@JsonDeserialize(using = RoutingRequestDeserializer.class)
public class RoutingRequest {

    private Point startNode;
    private Point endNode;

    private Set<WeatherTag> forbidden;

    private int startDayAfterToday;
    private int endDayAfterToday;

    public RoutingRequest(Point startNode, Point endNode, Set<WeatherTag> forbidden, int startDayAfterToday, int endDayAfterToday) {
        this.startNode = startNode;
        this.endNode = endNode;
        this.forbidden = forbidden;
        this.startDayAfterToday = startDayAfterToday;
        this.endDayAfterToday = endDayAfterToday;
    }


    public Point getStartNode() {
        return startNode;
    }

    public Point getEndNode() {
        return endNode;
    }

    public Set<WeatherTag> getForbidden() {
        return forbidden;
    }

    public int getStartDayAfterToday() {
        return startDayAfterToday;
    }

    public int getEndDayAfterToday() {
        return endDayAfterToday;
    }

    public void setStartNode(Point startNode) {
        this.startNode = startNode;
    }

    public void setEndNode(Point endNode) {
        this.endNode = endNode;
    }

    public void setStartDayAfterToday(int startDayAfterToday) {
        this.startDayAfterToday = startDayAfterToday;
    }

    public void setEndDayAfterToday(int endDayAfterToday) {
        this.endDayAfterToday = endDayAfterToday;
    }


    @Override
    public String toString() {
        String startNodeStr = "\"startNode\": " + startNode.toString();
        String endNodeStr = "\"endNode\": " + endNode.toString();
        String forbiddenStr = "\"forbidden\": [" + forbidden.stream()
                .map(WeatherTag::toString)
                .collect(joining(",")) + "]";
        String startDayAfterTodayStr = "\"startDayAfterToday\": " + startDayAfterToday;
        String endDayAfterTodayStr = "\"endDayAfterToday\": " + endDayAfterToday;

        return String.format("{%s, %s, %s, %s, %s}", startNodeStr, endNodeStr, forbiddenStr, startDayAfterTodayStr, endDayAfterTodayStr);
    }
}
