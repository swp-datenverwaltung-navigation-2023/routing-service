package de.fuberlin.navigator.routingserver.model;

public class AddressMatchingRequest {

    String address;

    public AddressMatchingRequest(String address) {
        this.address = address;
    }

    public AddressMatchingRequest() {
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "{\"address\":" + address + "}";
    }
}
