package de.fuberlin.navigator.routingserver.utility;

import de.fuberlin.navigator.protos.map_builder.Coordinates;

public class GeoUtils {
    // distance is returned in meters
    public static int haversine(Coordinates position_0, Coordinates position_1) {
        float lat_0 = position_0.getLat();
        float lon_0 = position_0.getLon();

        float lat_1 = position_1.getLat();
        float lon_1 = position_1.getLon();

        return GeoUtils.haversine(lat_0, lon_0, lat_1, lon_1);
    }

    // distance is returned in meters
    public static int haversine(float lat1, float lon1, float lat2, float lon2) {
        final int R = 6371 * 1000; // Radious of the earth
        float latDistance = toRad(lat2 - lat1);
        float lonDistance = toRad(lon2 - lon1);
        float a = (float) (Math.sin(latDistance / 2) * Math.sin(latDistance / 2) +
                Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) *
                        Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2));
        float c = (float) (2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)));
        return Math.round(R * c);
    }

    private static float toRad(float value) {
        return (float) (value * Math.PI / 180);
    }

}

