package de.fuberlin.navigator.routingserver.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class RoutingResponse {

    private List<Point> coordinates;
    //distance in meter
    private int distance;
    //duration in minutes
    private int duration;
    //difference to day of optimal route
    private int daysAfterToday;
    // 0 -> everything okay
    // 1 -> no route was found
    private int errorCode;

    private RoutingResponse(List<Point> coordinates, int distance, int duration, int daysAfterToday, int errorCode) {
        this.coordinates = coordinates;
        this.distance = distance;
        this.duration = duration;
        this.daysAfterToday = daysAfterToday;
        this.errorCode = errorCode;
    }

    public static RoutingResponse of(List<Point> coordinates, int distance, int duration, int daysAfterToday) {
        return new RoutingResponse(coordinates, distance, duration, daysAfterToday, 0);
    }

    public static RoutingResponse noPathFound() {
        return new RoutingResponse(null, -1, -1, -1, 1);
    }

    public JSONObject toJson() {
        JSONObject jsonObject = new JSONObject();

        if (errorCode == 1) {
            jsonObject.put("route", new JSONArray());
            jsonObject.put("distance", -1);
            jsonObject.put("duration", -1);
            jsonObject.put("days_after_today", -1);
            jsonObject.put("error_code", 1);
            return jsonObject;
        }

        JSONArray route = new JSONArray();
        for (Point coordinate : this.coordinates) {
            route.put(
                    new JSONArray().put(coordinate.getLat())
                            .put(coordinate.getLon()));
        }
        jsonObject.put("route", route);
        jsonObject.put("duration", this.duration);
        jsonObject.put("distance", this.distance);
        jsonObject.put("days_after_today", this.daysAfterToday);
        jsonObject.put("error_code", 0);

        return jsonObject;
    }


}
