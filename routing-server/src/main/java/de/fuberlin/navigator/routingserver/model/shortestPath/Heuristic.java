package de.fuberlin.navigator.routingserver.model.shortestPath;

import org.jgrapht.alg.interfaces.AStarAdmissibleHeuristic;

import com.graphhopper.util.DistanceCalcEarth;

import de.fuberlin.navigator.protos.map_builder.Coordinates;
import de.fuberlin.navigator.routingserver.utility.NetworkReader;
import de.fuberlin.navigator.routingserver.utility.ProtobufToGraph;

public class Heuristic {
    public ProtobufToGraph mapper;

    public Heuristic(ProtobufToGraph mapper) {
        this.mapper = mapper;
    }

    public static void main(String[] args) {
        Heuristic heuristic = new Heuristic(new ProtobufToGraph(NetworkReader.readRoadNetwork()));
        System.out.println(heuristic);
    }

    // create an instance of AStarAdmissibleHeuristic
    public AStarAdmissibleHeuristic<Long> heuristic = new AStarAdmissibleHeuristic<Long>() {
        @Override
        public double getCostEstimate(Long source, Long target) {

            double lat1 = getLocationX(source);
            double lon1 = getLocationY(source);
            double lat2 = getLocationX(target);
            double lon2 = getLocationY(target);

            DistanceCalcEarth distCalc = new DistanceCalcEarth();

            // calculate haversian distance between current node and goal
            double distance = distCalc.calcDist(lat1, lon1, lat2, lon2);

            return distance;
        }

        // helper methods to get latitude and longitude of a vertex
        private double getLocationX(long vertex) {
            Coordinates coordinates = mapper.getNodeMap().get(vertex);
            return coordinates.getLat();
        }

        private double getLocationY(long vertex) {
            Coordinates coordinates = mapper.getNodeMap().get(vertex);
            return coordinates.getLon();
        }

    };

}

