package de.fuberlin.navigator.routingserver.utility;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import de.fuberlin.navigator.routingserver.model.AddressMatchingRequest;
import de.fuberlin.navigator.routingserver.model.AddressMatchingResponse;
import de.fuberlin.navigator.routingserver.model.Point;
import org.json.JSONObject;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.operation.distance.DistanceOp;

import de.fuberlin.navigator.protos.map_builder.Coordinates;
import de.fuberlin.navigator.protos.map_builder.RoadNetwork;
import de.fuberlin.navigator.protos.map_builder.Segment;
import de.fuberlin.navigator.routingserver.model.MatchedPoint;

import static de.fuberlin.navigator.routingserver.utility.Config.*;

public class MapMatcher {

    private static final String NOMINATIM_ENDPOINT = "https://nominatim.openstreetmap.org/search?";
    private static final String FORMAT_PARAM = "format=json";
    private final RoadNetwork roadnetwork;
    private static final GeometryFactory geometryFactory = new GeometryFactory();

    public MapMatcher(RoadNetwork roadNetwork) {
        this.roadnetwork = roadNetwork;
    }

    public static void main(String[] args) throws IOException {
        RoadNetwork roadNetwork = NetworkReader.readRoadNetwork();
        MapMatcher matcher = new MapMatcher(roadNetwork);
        matcher.matchLocationToRoadnetwork(51.756329, 14.325981);
    }

    public static AddressMatchingResponse generateAddressMatchingResponse(AddressMatchingRequest request) {
        Point coordinate = getCoordinate(request.getAddress());

        if (coordinate == null) {
            return AddressMatchingResponse.addressNotFound();
        }

        if (coordinate.getLat() < BOUNDING_BOX_MIN_LAT || coordinate.getLat() > BOUNDING_BOX_MAX_LAT
                || coordinate.getLon() < BOUNDING_BOX_MIN_LON || coordinate.getLon() > BOUNDING_BOX_MAX_LON) {
            //coordinate is not in our road network
            return AddressMatchingResponse.addressNotInRoadNetwork();
        }

        return AddressMatchingResponse.of(coordinate);
    }

    public static Point getCoordinate(String address) {

        String addressParam = address.replaceAll(" ", "%20");
        // Construct the request URL
        String requestUrl = NOMINATIM_ENDPOINT + "q=" + addressParam + "&" + FORMAT_PARAM;

        // Send the request and parse the response
        String responseString = null;
        try {
            URL url = new URL(requestUrl);
            HttpURLConnection connection = null;
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            InputStream responseStream = connection.getInputStream();
            responseString = new String(responseStream.readAllBytes(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        // Removing first and last character
        // because response is in []
        responseString = responseString.substring(1, responseString.length() - 1);
        if (responseString.isEmpty()) {
            return null;
        }
        // Extract the latitude and longitude from the response
        JSONObject responseJson = new JSONObject(responseString);
        if (!responseJson.has("lat") || !responseJson.has("lon")) {
            return null;
        }

        double latitude = Double.parseDouble(responseJson.getString("lat"));
        double longitude = Double.parseDouble(responseJson.getString("lon"));

        return new Point(latitude, longitude);
    }

    /*
     * Function which returns the object or set of objects,
     * which is the start point
     * for the roouting algorithm.
     */
    public MatchedPoint matchLocationToRoadnetwork(double lat, double lon) {
        Map<Long, Segment> segments = this.roadnetwork.getSegmentsMap();
        MatchedPoint point = new MatchedPoint(0, Coordinates.newBuilder().build(), 0);
        double minimalDistance = Double.POSITIVE_INFINITY;
        Coordinate pointToMatch = new Coordinate(lon, lat);
        for (var segment : segments.entrySet()) {
            List<Coordinates> segmentCoordinates = segment.getValue().getGeometryList();
            Coordinate[] jtsCoordinates = protoToJTSCoordinates(segmentCoordinates);
            Coordinates matchedPoint = jstMatchPoint(jtsCoordinates, pointToMatch);
            int distance = GeoUtils.haversine((float) lat, (float) lon, matchedPoint.getLat(), matchedPoint.getLon());
            if (distance < minimalDistance) {
                // smallest distance to the segment
                minimalDistance = distance;
                // set matched point values
                point.setSegmentId(segment.getKey());
                point.setCoordinates(matchedPoint);
                point.setStartNode(segment.getValue().getStartNode());
                point.setEndNode(segment.getValue().getEndNode());
                // get segment offset
                float segment_lat = segment.getValue().getGeometry(0).getLat();
                float segment_lon = segment.getValue().getGeometry(0).getLon();
                float point_lat = point.getCoordinates().getLat();
                float point_lon = point.getCoordinates().getLon();
                float distanceOnSegment = GeoUtils.haversine(point_lat, point_lon, segment_lat, segment_lon);
                double offset = distanceOnSegment / segment.getValue().getLength();
                point.setOffset(offset);
            }
        }
        System.out.println("The closest point is: " + point.getCoordinates() + " with offset: " + point.getOffset()
                + " and segment ID; " + point.getSegmentId());
        return point;
    }

    private static Coordinate[] protoToJTSCoordinates(List<Coordinates> coords) {
        Coordinate[] coordinateList = new Coordinate[coords.size()];
        for (int i = 0; i < coords.size(); i++) {
            Coordinate toInsert = new Coordinate(coords.get(i).getLon(), coords.get(i).getLat());
            coordinateList[i] = toInsert;
        }
        return coordinateList;
    }

    public static Coordinates jstMatchPoint(Coordinate[] coordinates, Coordinate pointToMatch) {
        LineString polygonFromCoordinates = geometryFactory.createLineString(coordinates);
        org.locationtech.jts.geom.Point point = geometryFactory.createPoint(pointToMatch);
        DistanceOp distOp = new DistanceOp(polygonFromCoordinates, point);
        Coordinate[] closestPt = distOp.nearestPoints();
        Coordinates returnCoords = Coordinates.newBuilder().setLat((float) closestPt[0].y)
                .setLon((float) closestPt[0].x).build();
        return returnCoords;

    }
}

