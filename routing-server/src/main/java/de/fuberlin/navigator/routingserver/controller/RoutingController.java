package de.fuberlin.navigator.routingserver.controller;

import java.io.IOException;

import de.fuberlin.navigator.routingserver.model.AddressMatchingResponse;
import de.fuberlin.navigator.routingserver.model.RoutingResponse;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ObjectNode;

import de.fuberlin.navigator.protos.map_builder.Coordinates;
import de.fuberlin.navigator.routingserver.model.AddressMatchingRequest;
import de.fuberlin.navigator.routingserver.model.RoutingRequest;
import de.fuberlin.navigator.routingserver.utility.ApplyShortestPath;
import de.fuberlin.navigator.routingserver.utility.MapMatcher;

@RestController
public class RoutingController {

        /**
         * Answers with a list of coordinates, where each
         * coordinate is a list of two elements. Also
         * contains duration, date and distance of the
         * found route. Will send an error code if
         * anything went wrong
         * 
         * @return JSON of list of coordinates
         */
        @PostMapping(value = "/route", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
        public String route(@RequestBody RoutingRequest routingRequest) {
                log("/route", RequestMethod.POST, MediaType.APPLICATION_JSON, routingRequest);

                // route generation
                RoutingResponse response = ApplyShortestPath.generateRoutingResponse(routingRequest);

                return response.toJson().toString();
        }

        /**
         * Answers with a coordinate pair. Sends an error_code if anything
         * went wrong
         * 
         * @return JSON coordinate pair
         */
        @PostMapping(value = "/addressmatching", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
        public String addressmatching(@RequestBody AddressMatchingRequest addressRequest) {
                log("/addressmatching", RequestMethod.POST, MediaType.APPLICATION_JSON, addressRequest);

                // resolve address with map matching method
                AddressMatchingResponse response = MapMatcher.generateAddressMatchingResponse(addressRequest);

                return response.toJson().toString();
        }

        private void log(String endpoint, RequestMethod method, MediaType type, Object requestBody) {
                System.out.println(String.format("""
                                Received the following request:
                                    Endpoint:   %s
                                    Method:     %s
                                    Media type: %s
                                    Body:       %s
                                """, endpoint, method, type, requestBody));
        }
}
