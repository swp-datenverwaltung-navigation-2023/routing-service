import requests


class RoutingEngine:
    starting_point: [float, float]
    end_point: [float, float]

    def __init__(self, url):
        self.api_url = url

        self.starting_point = None
        self.end_point = None

    def clear_points(self):
        self.starting_point = None
        self.end_point = None

    def set_starting_point(self, start):
        self.starting_point = start

    def set_end_point(self, end):
        self.end_point = end

    def get_starting_point(self):
        return self.starting_point

    def get_end_point(self):
        return self.end_point

    def fetch_route(self):
        start = self.starting_point
        end = self.end_point

        headers = {
            'cache-control': 'private, max-age=0, no-cache'
        }
        request_url = self.api_url.format(start[0], start[1], end[0], end[1])

        response = requests.get(request_url, headers=headers)

        if response.ok:
            return response.json()
