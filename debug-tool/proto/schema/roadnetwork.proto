syntax = "proto3";

package protos;

option java_multiple_files = true;
option java_package = "map.builder.protos";
option java_outer_classname = "RoadNetworkProto";

//Roadnetwork represents our whole roadnetwork
message RoadNetwork {
    // Nodes are corresponding to intersections, the key value currently is the OSM id
    map<uint64,Node> nodes = 1;
    // Segments correspond to edges in our graph, the key value currently is the OSM id
    map<uint64,Segment> segments = 2;
    // Turn restrictitons correspond to turn restrictions
    repeated Restriction turn_restrictions = 3;
}

// Nodes represent usually intersections in our network.
message Node { 
    // Internally used ids of our segments, currently same as OSM id
    uint64 id = 1;
    // Vendor IDs of OSM data, used for referencing the original data
    uint64 osm_id = 2;
    // Coordinaes of the intersection
    Coordinates position = 3;
}

// Segment represents a directed edge in our network, which connects two nodes
message Segment {
    // id is the internal id used in our system of a segment, currently same as OSM id
    uint64 id = 1;
    // osm_id is the id of the segment, in the original OSM data, if it exists as a separate segment there
    uint64 osm_id = 2;
    // geometry represents the linestring representing the segment
    repeated Coordinates geometry = 3;
    // length of the segment in meters
    double length = 4;
    // start_node is the start endpoint of the segment
    uint64 start_node = 5;
    // end_node is the end endpoint of the segment
    uint64 end_node = 6;
    // category of the road according to OSM main road types
    RoadCategory category = 7;
}

// RoadCategory groups road segments into several classes
enum RoadCategory {
    // a special value to indicate that the field is not set
    ROAD_CATEGORY_INVALID = 0;
    // represents motorway and trunks, according to the specification of OSM's data
    ROAD_CATEGORY_HIGHWAY = 1;
    // often link larger towns
    ROAD_CATEGORY_MAIN = 2;
    // represents a road within a city or connecting small towns
    ROAD_CATEGORY_LOCAL = 3;
    // represents a vehicular access to small streets connecting roads with apartments, buildings
    ROAD_CATEGORY_RESIDENTIAL=4;
}

// A restricted turn from one segment to another, which are adjacent to each other
message Restriction {
    // from_id is the id of the segment from which the turn originates
    uint64 from_id = 1;
    // to_id is the id of the segment in which the turn ends
    uint64 to_id = 2;
}

// A location is geographical point. The coordinate system used is WGS 84
message Coordinates { 
    // lat corresponds to the latitude of the point
    float lat = 1;
    // lon corresponds to the longitude of the point
    float lon = 2;
}
