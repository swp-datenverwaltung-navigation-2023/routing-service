package de.fuberlin.navigator.routingserver;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = {RoutingServerApplication.class})
class RoutingServerApplicationTests {

    @Test
    public void contextLoad() {

    }

}
