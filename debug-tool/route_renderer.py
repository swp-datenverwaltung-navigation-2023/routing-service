from osrm_routing_engine import OSRMRoutingEngine
from ipyleaflet import LayerGroup, AwesomeIcon, Marker, Polyline
from own_routing_engine import OwnRoutingEngine


class RouteRenderer:
    def __init__(self):
        self.routing_engine_osrm = OSRMRoutingEngine()
        self.own_routing_engine = OwnRoutingEngine()

        self.layer = LayerGroup()

        self.__ready_to_build_route = False
        self.__route_built = False

        self.start_marker = None
        self.end_marker = None
        self.osrm_route_line = None
        self.own_route_line = None

    def get_layer(self):
        return self.layer

    def has_built_route(self):
        return self.__route_built

    def clear_points(self):
        self.__ready_to_build_route = False

        self.routing_engine_osrm.clear_points()
        self.own_routing_engine.clear_points()

        if self.end_marker:
            self.layer.remove(self.end_marker)
            self.end_marker = None
        if self.start_marker:
            self.layer.remove(self.start_marker)
            self.start_marker = None

    def clear_route(self):
        self.__route_built = False
        self.__ready_to_build_route = False

        self.layer.remove(self.osrm_route_line)
        self.layer.remove(self.own_route_line)

    def build_route(self):
        route = self.routing_engine_osrm.fetch_route()
        route2 = self.own_routing_engine.fetch_route()

        self.osrm_route_line = Polyline(
            locations=route,
            color="red",
            opacity=0.3,
            fill=False,
        )
        self.own_route_line = Polyline(
            locations=route2,
            color="blue",
            opacity=0.8,
            fill=False,
        )
        # layer.on_click(self.onEdgeClick(row))
        self.layer.add(self.osrm_route_line)
        self.layer.add(self.own_route_line)
        self.__route_built = True

    def is_ready_to_build_route(self):
        return self.__ready_to_build_route

    def add_point(self, point):
        if self.routing_engine_osrm.get_starting_point() is None:
            self.__set_starting_point(point)
            self.__add_start_marker(point)
        elif self.routing_engine_osrm.get_end_point() is None:
            self.__set_ending_point(point)
            self.__add_end_marker(point)
            self.__ready_to_build_route = True

    def __set_starting_point(self, point):
        self.own_routing_engine.set_starting_point(point)
        self.routing_engine_osrm.set_starting_point(point)
    def __set_ending_point(self, point):
        self.own_routing_engine.set_end_point(point)
        self.routing_engine_osrm.set_end_point(point)

    def __add_end_marker(self, location):
        icon = AwesomeIcon(
            marker_color="red",
            name="bus"
        )

        self.end_marker = self.__add_marker(location, icon)

    def __add_start_marker(self, location):
        self.start_marker = self.__add_marker(location)

    def __clear_route(self):
        self.layer.clear()
        self.routing_engine_osrm.clear_points()

    def __add_marker(self, location, icon=None):
        marker = Marker(location=location, icon=icon)
        self.layer.add(marker)

        return marker
