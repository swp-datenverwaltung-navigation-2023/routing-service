
package de.fuberlin.navigator.routingserver.utility;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import de.fuberlin.navigator.protos.map_builder.Coordinates;
import de.fuberlin.navigator.protos.map_builder.Node;
import de.fuberlin.navigator.protos.map_builder.RoadNetwork;
import de.fuberlin.navigator.protos.map_builder.Segment;
import de.fuberlin.navigator.protos.metric_builder.Metric;
import de.fuberlin.navigator.routingserver.model.shortestPath.ExtendedEdge;

public class ProtobufToGraph {
    static Random rd = new Random();
    private HashMap<Long, Coordinates> nodes;
    private HashMap<Long, Segment> segments;

    public ProtobufToGraph(RoadNetwork roadNetwork) {
        this.nodes = parseNodes(roadNetwork.getNodesMap());
        this.segments = parseSegments(roadNetwork.getSegmentsMap());
    }

    private HashMap<Long, Coordinates> parseNodes(Map<Long, Node> nodes) {
        HashMap<Long, Coordinates> map = new HashMap<Long, Coordinates>();
        for (Long key : nodes.keySet()) {
            map.put(nodes.get(key).getOsmId(), nodes.get(key).getPosition());
        }
        return map;
    }

    private HashMap<Long, Segment> parseSegments(Map<Long, Segment> segments) {
        HashMap<Long, Segment> map = new HashMap<Long, Segment>();
        for (Long key : segments.keySet()) {
            map.put(key, segments.get(key));
        }
        return map;
    }

    // Used to generate ids for the edge
    public long generateSegmentID() {
        long id = rd.nextLong();
        while (this.segments.keySet().contains(id)) {
            id = rd.nextLong();
        }
        return id;
    }

    public void addNode(Long id, Coordinates coords) {
        this.nodes.put(id, coords);
    }

    public void addSegment(Long id, Segment segment) {
        this.segments.put(id, segment);
    }

    public void removeSegment(Long id) {
        this.segments.remove(id);
    }

    public Map<Long, Coordinates> getNodeMap() {
        return this.nodes;
    }

    public static void main(String[] args) {
        RoadNetwork network = NetworkReader.readRoadNetwork();
        ProtobufToGraph test = new ProtobufToGraph(network);
        test.getGraphFromProto(0);
    }

    public SimpleDirectedWeightedGraph<Long, ExtendedEdge> getGraphFromProto(int dayAfterToday){


        // create a new directed weighted graph
        SimpleDirectedWeightedGraph<Long, ExtendedEdge> graph = new SimpleDirectedWeightedGraph<>(ExtendedEdge.class);

        // add vertices to the graph
        for (Map.Entry<Long, Coordinates> node : nodes.entrySet()) {
            graph.addVertex(node.getKey());
        }



        // add edges to the graph
        for (Map.Entry<Long,Segment> edge : segments.entrySet()) {
            long source = edge.getValue().getStartNode();
            long target = edge.getValue().getEndNode();

            double weight = calculateWeight(edge.getValue(), dayAfterToday);
            
            ExtendedEdge e1 = new ExtendedEdge(edge.getValue().getId());
            ExtendedEdge e2 = new ExtendedEdge(edge.getValue().getId());
            try {
                graph.addEdge(source, target, e1);
                graph.setEdgeWeight(e1, weight);

                if (!this.segments.get(edge.getKey()).getOneWay()) {
                    graph.addEdge(target, source, e2);
                    graph.setEdgeWeight(e2, weight);
                }
            } catch (Exception exception) {
                System.out.println(exception);
                System.out.println("ID :" + edge.getValue().getId() + " Start node: " + edge.getValue().getStartNode()
                        + " End node:" + edge.getValue().getEndNode());
            }
        }

        return graph;
    }

    private static double calculateWeight(Segment edge, int dayAfterToday){
        Metric metric = MetricReader.metrics.getMetricsMap().get(dayAfterToday).getMetricsPerDayMap().get(edge.getId());
        double maxSpeed = metric.getTravelSpeed();
        double lengthKm = edge.getLength()/1000;
        double weight = lengthKm/maxSpeed;
        return weight;
    }
}    
