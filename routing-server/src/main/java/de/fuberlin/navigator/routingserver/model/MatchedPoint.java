package de.fuberlin.navigator.routingserver.model;

import de.fuberlin.navigator.protos.map_builder.Coordinates;

public class MatchedPoint {
    private long segmentId;
    private Coordinates coordinates;
    private double offset;
    private long start_node;
    private long end_node;

    public MatchedPoint(long segmentId, Coordinates coordinates, double offset) {
        this.segmentId = segmentId;
        this.coordinates = coordinates;
        this.offset = offset;
    }

    public long getSegmentId() {
        return this.segmentId;
    }

    public Coordinates getCoordinates() {
        return this.coordinates;
    }

    public double getOffset() {
        return this.offset;
    }

    public long getStartNode() {
        return this.start_node;
    }

    public long getEndNode() {
        return this.end_node;
    }

    public void setSegmentId(long segmentId) {
        this.segmentId = segmentId;
    }

    public void setCoordinates(Coordinates coords) {
        this.coordinates = coords;
    }

    public void setOffset(double offset) {
        this.offset = offset;
    }

    public void setStartNode(long start_node) {
        this.start_node = start_node;
    }

    public void setEndNode(long end_node) {
        this.end_node = end_node;
    }
}
