package de.fuberlin.navigator.routingserver.model.shortestPath;

import org.jgrapht.graph.DefaultWeightedEdge;

public class ExtendedEdge extends DefaultWeightedEdge {

    private Long id;

    /**
     * Constructs an edge with id
     */
    public ExtendedEdge(Long id)
    {
        this.id = id;
    }

    /**
     * Gets the id associated with this edge.
     */
    public Long getId()
    {
        return id;
    }


}










