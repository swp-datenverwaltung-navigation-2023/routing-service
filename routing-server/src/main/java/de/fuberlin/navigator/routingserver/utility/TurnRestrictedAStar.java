package de.fuberlin.navigator.routingserver.utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import org.jgrapht.alg.shortestpath.AStarShortestPath;


import org.jgrapht.*;
import org.jgrapht.alg.interfaces.*;
import org.jgrapht.graph.*;
import org.jgrapht.util.*;

import de.fuberlin.navigator.protos.map_builder.Restriction;
import de.fuberlin.navigator.protos.metric_builder.Metric;
import de.fuberlin.navigator.protos.metric_builder.WeatherCondition;
import de.fuberlin.navigator.routingserver.model.WeatherTag;
import de.fuberlin.navigator.routingserver.model.shortestPath.ExtendedEdge;


public class TurnRestrictedAStar<V, E> extends AStarShortestPath<V, E> {

    /*
     * Sub class of AStarShortestPath to check for turn and weather restrictions.
     * Overriding 'getPath' method to add restriction checks in 'expandNode'
     * only code added are the methods 'isTurnRestricted' and 'isForbiddenWeather'
     * and corresponding checks in method 'expandNode' (line 120, 128) to filter
     * out affected edges
     */

    private Set<WeatherTag> forbidden;
    private int dayAfterToday;

    public TurnRestrictedAStar(Graph<V, E> graph, AStarAdmissibleHeuristic<V> admissibleHeuristic) {
        super(graph, admissibleHeuristic);
    }

    public TurnRestrictedAStar(Graph<V, E> graph, AStarAdmissibleHeuristic<V> admissibleHeuristic, Set<WeatherTag> forbidden, int dayAfterToday) {
        super(graph, admissibleHeuristic);
        this.forbidden = forbidden;
        this.dayAfterToday = dayAfterToday;
    }

    private void initialize(AStarAdmissibleHeuristic<V> admissibleHeuristic)
    {
        this.admissibleHeuristic = admissibleHeuristic;
        openList = new FibonacciHeap<>();
        vertexToHeapNodeMap = new HashMap<>();
        closedList = new HashSet<>();
        gScoreMap = new HashMap<>();
        cameFrom = new HashMap<>();
        numberOfExpandedNodes = 0;
    }


    @Override
    public GraphPath<V, E> getPath(V sourceVertex, V targetVertex)
    {
        if (!graph.containsVertex(sourceVertex) || !graph.containsVertex(targetVertex)) {
            throw new IllegalArgumentException(
                "Source or target vertex not contained in the graph!");
        }

        if (sourceVertex.equals(targetVertex)) {
            return createEmptyPath(sourceVertex, targetVertex);
        }

        this.initialize(admissibleHeuristic);
        gScoreMap.put(sourceVertex, 0.0);
        FibonacciHeapNode<V> heapNode = new FibonacciHeapNode<>(sourceVertex);
        openList.insert(heapNode, 0.0);
        vertexToHeapNodeMap.put(sourceVertex, heapNode);

        do {
            FibonacciHeapNode<V> currentNode = openList.removeMin();

            // Check whether we reached the target vertex
            if (currentNode.getData().equals(targetVertex)) {
                // Build the path
                return this.buildGraphPath(sourceVertex, targetVertex, currentNode.getKey());
            }

            // We haven't reached the target vertex yet; expand the node
            expandNode(currentNode, targetVertex);
            closedList.add(currentNode.getData());
        } while (!openList.isEmpty());

        // No path exists from sourceVertex to TargetVertex
        return createEmptyPath(sourceVertex, targetVertex);
    }

   
    private void expandNode(FibonacciHeapNode<V> currentNode, V endVertex)
    {
        numberOfExpandedNodes++;

        Set<E> outgoingEdges = null;
        if (graph instanceof UndirectedGraph) {
            outgoingEdges = graph.edgesOf(currentNode.getData());
        } else if (graph instanceof DirectedGraph) {
            outgoingEdges = ((DirectedGraph<V, E>) graph).outgoingEdgesOf(currentNode.getData());
        }

        for (E edge : outgoingEdges) {
            V successor = Graphs.getOppositeVertex(graph, edge, currentNode.getData());

            if (successor.equals(currentNode.getData())) { // Ignore self-loop
                continue;
            }

            // add turn restriction check
            E previous = cameFrom.get(currentNode.getData());
            if (previous!=null){
                if(isTurnRestricted(previous,edge)){
                    continue;
                }
            }

            // add forbidden weather check
            if(isForbiddenWeather(edge)){
                continue;
            }


            double gScore_current = gScoreMap.get(currentNode.getData());
            double tentativeGScore = gScore_current + graph.getEdgeWeight(edge);
            double fScore =
                tentativeGScore + admissibleHeuristic.getCostEstimate(successor, endVertex);

            if (vertexToHeapNodeMap.containsKey(successor)) { // We re-encountered a vertex. It's
                                                              // either in the open or closed list.
                if (tentativeGScore >= gScoreMap.get(successor)) // Ignore path since it is
                                                                 // non-improving
                    continue;

                cameFrom.put(successor, edge);
                gScoreMap.put(successor, tentativeGScore);

                if (closedList.contains(successor)) { // it's in the closed list. Move node back to
                                                      // open list, since we discovered a shorter
                                                      // path to this node
                    closedList.remove(successor);
                    openList.insert(vertexToHeapNodeMap.get(successor), fScore);
                } else { // It's in the open list
                    openList.decreaseKey(vertexToHeapNodeMap.get(successor), fScore);
                }
            } else { // We've encountered a new vertex.
                cameFrom.put(successor, edge);
                gScoreMap.put(successor, tentativeGScore);
                FibonacciHeapNode<V> heapNode = new FibonacciHeapNode<>(successor);
                openList.insert(heapNode, fScore);
                vertexToHeapNodeMap.put(successor, heapNode);
            }
        }
    }


    private GraphPath<V, E> buildGraphPath(V startVertex, V targetVertex, double pathLength)
    {
        List<E> edgeList = new ArrayList<>();
        List<V> vertexList = new ArrayList<>();
        vertexList.add(targetVertex);

        V v = targetVertex;
        while (!v.equals(startVertex)) {
            edgeList.add(cameFrom.get(v));
            v = Graphs.getOppositeVertex(graph, cameFrom.get(v), v);
            vertexList.add(v);
        }
        Collections.reverse(edgeList);
        Collections.reverse(vertexList);
        return new GraphWalk<>(graph, startVertex, targetVertex, vertexList, edgeList, pathLength);
    }

    private boolean isTurnRestricted(E edge1, E edge2){
        for (Restriction restriction : ApplyShortestPath.roadnetwork.getTurnRestrictionsList()) {
            if (restriction.getFromId() == ((ExtendedEdge) edge1).getId() && restriction.getToId() == ((ExtendedEdge) edge2).getId()){
                return true;

            }
        }
        return false;  

    }

    private boolean isForbiddenWeather(E edge){
        Metric metric = MetricReader.metrics.getMetricsMap().get(dayAfterToday).getMetricsPerDayMap().get((long)((ExtendedEdge) edge).getId());
        if(metric == null) return false;
        List<WeatherCondition> weather = metric.getAdditionalFactorsList();

        if (forbidden.contains(WeatherTag.ICE) && weather.contains(WeatherCondition.WEATHER_ICE)){
            return true;
        }
        if (forbidden.contains(WeatherTag.RAIN) && weather.contains(WeatherCondition.WEATHER_RAIN)){
            return true;
        }
        if (forbidden.contains(WeatherTag.SNOW) && weather.contains(WeatherCondition.WEATHER_SNOW)){
            return true;
        }
        if (forbidden.contains(WeatherTag.WIND) && weather.contains(WeatherCondition.WEATHER_WIND)){
            return true;
        }
        return false;  

    }

 

}

