from routing_engine import RoutingEngine
import requests
import json


class OwnRoutingEngine(RoutingEngine):
    def __init__(self):
        super().__init__('http://localhost:8080/route')

    def fetch_route(self):
        headers = {
            'Content-Type': 'application/json'
        }

        response = requests.post(self.api_url, headers=headers, data=self.__build_payload())
        parsed_response = response.json()

        if isinstance(parsed_response, dict):
            return parsed_response.get('route', [])

        return []

    def __build_payload(self):
        start = self.starting_point
        end = self.end_point

        payload = {
            'start_node': {
                'lat': start[0], 'lon': start[1]
            },
            'end_node': {
                'lat': end[0], 'lon': end[1]
            },
            'forbidden': [],
            'start_after_today': 0,
            'end_after_today': 6
        }
        return json.dumps(payload)
