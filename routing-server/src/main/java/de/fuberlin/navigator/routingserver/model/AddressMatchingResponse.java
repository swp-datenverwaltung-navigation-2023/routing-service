package de.fuberlin.navigator.routingserver.model;

import org.json.JSONObject;

public class AddressMatchingResponse {
    Point coordinate;
    // 0 -> okay
    // 1 -> address is not in our road network
    // 2 -> address not found
    int errorCode;

    private AddressMatchingResponse(Point coordinate, int errorCode) {
        this.coordinate = coordinate;
        this.errorCode = errorCode;
    }

    public static AddressMatchingResponse of(Point coordinate) {
        return new AddressMatchingResponse(coordinate, 0);
    }

    public static AddressMatchingResponse addressNotInRoadNetwork() {
        return new AddressMatchingResponse(null, 1);
    }

    public static AddressMatchingResponse addressNotFound() {
        return new AddressMatchingResponse(null, 2);
    }

    public JSONObject toJson() {
        JSONObject jsonObject = new JSONObject();

        if (this.errorCode != 0) {
            jsonObject.put("coordinate", "");
            jsonObject.put("error_code", this.errorCode);
            return jsonObject;
        }

        jsonObject.put("coordinate",
                new JSONObject().put("lat", this.coordinate.getLat())
                        .put("lon", this.coordinate.getLon())
        );
        jsonObject.put("error_code", 0);

        return jsonObject;
    }
}
