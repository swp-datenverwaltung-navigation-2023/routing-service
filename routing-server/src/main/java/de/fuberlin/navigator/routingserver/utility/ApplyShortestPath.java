package de.fuberlin.navigator.routingserver.utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import de.fuberlin.navigator.routingserver.model.*;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.AStarShortestPath;

import de.fuberlin.navigator.protos.map_builder.RoadNetwork;
import de.fuberlin.navigator.routingserver.model.shortestPath.ExtendedEdge;
import de.fuberlin.navigator.routingserver.model.shortestPath.Heuristic;
import de.fuberlin.navigator.protos.map_builder.Coordinates;
import de.fuberlin.navigator.protos.map_builder.Segment;


public class ApplyShortestPath {

    static Random rd = new Random();
    static MapMatcher matcher = new MapMatcher(NetworkReader.readRoadNetwork());
    static ProtobufToGraph mapper = new ProtobufToGraph(NetworkReader.readRoadNetwork());

    public static SimpleDirectedWeightedGraph<Long, ExtendedEdge> graph;

    public static RoadNetwork roadnetwork = NetworkReader.readRoadNetwork();



    public static void main(String[] args){

        // test routing algorithm
        Set<WeatherTag> weatherTags = new HashSet<>(){{
            add(WeatherTag.findByCode("rain"));
        }};

        double lat1 = 51.762146;
        double lon1 = 14.286304;
        double lat2 = 51.747814;
        double lon2 = 14.357037;

        RoutingRequest routingRequest = new RoutingRequest(new Point(lat1,lon1), new Point(lat2, lon2), weatherTags, 0, 6);

        // find the shortest path 
        GraphPath<Long, ExtendedEdge> path = getShortestPathInTime(routingRequest);

        List<Point> coordinateList = covertToCoordinates(path);

        // duration of path in minutes
        int duration = calculateDuration(path);

        // length of path in meters
        int length = calculateLength(path);

        System.out.println("List of Coordinates: "+Arrays.deepToString(coordinateList.toArray()));
        System.out.println("duration: " + duration + " min");
        System.out.println("length of path: " + length + " meter");
    }

    public static RoutingResponse generateRoutingResponse(RoutingRequest request) {
        GraphPath<Long, ExtendedEdge> path = null;

        int start = request.getStartDayAfterToday();
        int end = request.getEndDayAfterToday();
        int daysAfterToday = -1;
        for (int i = start; i <= end; i++ ){
            path = getShortestPath(request, i);
            if (path != null) {
                System.out.println("Path found in " + i + " days!");
                daysAfterToday = i;
                break;
            }
            System.out.println("No path with desired conditions could be found in " +i+ " days!");
        }

        if (path == null) {
            return RoutingResponse.noPathFound();
        }

        //in minutes
        int duration = calculateDuration(path);
        //in meters
        int distance = calculateLength(path);

        return RoutingResponse.of(covertToCoordinates(path), distance, duration, daysAfterToday);
    }

    private static GraphPath<Long, ExtendedEdge> getShortestPathInTime(RoutingRequest routingRequest){
        int start = routingRequest.getStartDayAfterToday();
        int end = routingRequest.getEndDayAfterToday();

        for (int i = start; i <= end; i++ ){
            GraphPath<Long, ExtendedEdge> path = getShortestPath(routingRequest, i);
            if (path != null) {
                System.out.println("Path found in " +i+ " days!");
                return path;
            }
            System.out.println("No path with desired conditions could be found in " +i+ " days!");
        }

        return null;

    }


    private static GraphPath<Long, ExtendedEdge> getShortestPath(RoutingRequest routingRequest, int dayAfterToday){

        graph = new ProtobufToGraph(NetworkReader.readRoadNetwork()).getGraphFromProto(dayAfterToday);

        double lat = routingRequest.getStartNode().getLat();
        double lon = routingRequest.getStartNode().getLon();
        MatchedPoint start_point = matcher.matchLocationToRoadnetwork(lat, lon);

        addNodeToGraph(start_point);

        lat = routingRequest.getEndNode().getLat();
        lon = routingRequest.getEndNode().getLon();
        MatchedPoint end_point = matcher.matchLocationToRoadnetwork(lat, lon);

        addNodeToGraph(end_point);

        // create an instance of AStarShortestPath
        Heuristic heuristic = new Heuristic(mapper);
        AStarShortestPath<Long, ExtendedEdge> aStar = new TurnRestrictedAStar<>(graph, heuristic.heuristic, routingRequest.getForbidden(), dayAfterToday);
        
        // find the shortest path 
        GraphPath<Long, ExtendedEdge> path = aStar.getPath(start_point.getSegmentId(),end_point.getSegmentId());


        // cleanup the graph
        removeNodeFromGraph(start_point);
        removeNodeFromGraph(end_point);

 
        return path;
    }

    /*
        * This function adds a vertex with the matched coordinates in the graph
        * and takes it as a start point/end point of the route.
        */
    private static void addNodeToGraph(MatchedPoint point) {
        // get the three ids for the segment
        Segment segment = roadnetwork.getSegmentsOrThrow(point.getSegmentId());
        long start_node = segment.getStartNode();
        long end_node = segment.getEndNode();
        // get the size of coordinates in a segment
        int geometry_length = segment.getGeometryCount() - 1;
        // get the start coordinates and end coordinates of the segment.
        // TODO: use the offset and take all the coordinates after the offset or before
        // the offset
        Coordinates start_coords = Coordinates.newBuilder().setLat(segment.getGeometry(0).getLat())
                        .setLon(segment.getGeometry(0).getLon()).build();
        Coordinates end_coords = Coordinates.newBuilder().setLat(segment.getGeometry(geometry_length).getLat())
                        .setLon(segment.getGeometry(geometry_length).getLon()).build();
        // remove edges from graph and mapper
        graph.removeEdge(start_node, end_node);
        graph.removeEdge(end_node, start_node);
        // add vertex to graph and mapper
        graph.addVertex(point.getSegmentId());
        Coordinates coords = Coordinates.newBuilder().setLat(point.getCoordinates().getLat())
                        .setLon(point.getCoordinates().getLon()).build();
        mapper.addNode(point.getSegmentId(), coords);
        // add the new edges
        Segment segment_to_add;
        ExtendedEdge e1 = new ExtendedEdge(mapper.generateSegmentID());
        segment_to_add = Segment.newBuilder().setId(e1.getId()).addGeometry(start_coords)
                        .addGeometry(coords).setStartNode(start_node).setEndNode(point.getSegmentId()).build();
        graph.addEdge(start_node, point.getSegmentId(), e1);
        mapper.addSegment(e1.getId(), segment);
        graph.setEdgeWeight(e1, segment.getLength()/1000/segment.getMaxSpeed() * point.getOffset());
        // add the second edge
        ExtendedEdge e2 = new ExtendedEdge(mapper.generateSegmentID());
        graph.addEdge(point.getSegmentId(), end_node, e2);
        segment_to_add = Segment.newBuilder().setId(e2.getId()).addGeometry(coords).addGeometry(end_coords)
                        .setStartNode(point.getSegmentId()).setEndNode(end_node)
                        .build();
        mapper.addSegment(e2.getId(), segment);
        graph.setEdgeWeight(e2, (int) segment.getLength()/1000/segment.getMaxSpeed() * (1 - point.getOffset()));
        // if the segment is 2 way segment, add it also in the other direction
        if (!segment.getOneWay()) {
            ExtendedEdge e3 = new ExtendedEdge(mapper.generateSegmentID());
            graph.addEdge(end_node, point.getSegmentId(), e3);
            segment_to_add = Segment.newBuilder().setId(e3.getId()).addGeometry(end_coords)
                            .addGeometry(coords)
                            .setStartNode(end_node).setEndNode(point.getSegmentId())
                            .build();
            mapper.addSegment(e3.getId(), segment_to_add);
            graph.setEdgeWeight(e3, (int) (1 - point.getOffset()) * segment.getLength()/1000/segment.getMaxSpeed());
            ExtendedEdge e4 = new ExtendedEdge(mapper.generateSegmentID());
            graph.addEdge(point.getSegmentId(), start_node, e4);
            segment_to_add = Segment.newBuilder().setId(e4.getId()).addGeometry(coords)
                            .addGeometry(start_coords)
                            .setStartNode(point.getSegmentId()).setEndNode(end_node)
                            .build();
            mapper.addSegment(e4.getId(), segment_to_add);
            graph.setEdgeWeight(e4, (int) segment.getLength()/1000/segment.getMaxSpeed() * point.getOffset());
        }
    }

    /*
     * After the algorithm has executed, remove the point with the matched
     * start and end nodes from the graph.
     */
    private static void removeNodeFromGraph(MatchedPoint point) {
        Set<ExtendedEdge> edges = graph.edgesOf(point.getSegmentId());
        for (ExtendedEdge edge : edges) {
                // cleanup
                mapper.removeSegment(edge.getId());
        }
        graph.removeAllEdges(edges);
        ExtendedEdge e1 = new ExtendedEdge(point.getSegmentId());
        graph.addEdge(point.getStartNode(), point.getEndNode(), e1);
        // if the edges are 2 then the street is only oneway and we don't need edge in
        // the other directiton
        if (edges.size() > 2) {
                ExtendedEdge e2 = new ExtendedEdge(mapper.generateSegmentID());
                graph.addEdge(point.getEndNode(), point.getStartNode(), e2);
        }
    }

    private static int calculateLength(GraphPath<Long, ExtendedEdge> path){
        double length = 0;
        for (ExtendedEdge edge : path.getEdgeList()) {
    
            // filtering out the newly generated edges because length cannot be accessed
            if(roadnetwork.getSegmentsMap().get(edge.getId()) == null) continue;

            double edgeLen = roadnetwork.getSegmentsMap().get(edge.getId()).getLength();
            length += edgeLen;    
        }

        return (int)length;
    }

    private static int calculateDuration(GraphPath<Long, ExtendedEdge> path){
        double durationH = path.getWeight();
        int durationMin = (int) (durationH * 60);
        return durationMin;
    }

    private static List<Point> covertToCoordinates(GraphPath<Long, ExtendedEdge> path){
        // convert path into path of Coordinates
        List<Point> coordinateList = new ArrayList<>();

        // add coordinates of generated start node
        Long startNodeID = path.getStartVertex();
        double lat = mapper.getNodeMap().get(startNodeID).getLat();
        double lon = mapper.getNodeMap().get(startNodeID).getLon();
        coordinateList.add(new Point(lat, lon));

        // add coordinates of edges
        int index = 0;
        for (ExtendedEdge edge : path.getEdgeList()) {
            // filter out generated first and last edge, which have no geometryList
            if (index == 0 || index == path.getEdgeList().size()-1) {
                index ++;
                continue;
            }
      
            Segment segment = roadnetwork.getSegmentsMap().get(edge.getId());
            List<Coordinates> geometryList= segment.getGeometryList();

            // handle reversed edges (the edges that are added if the street is no oneway)
            // find right geometryList order by calculating distance between last element of coordinateList and geometryList
            Point last = coordinateList.get(coordinateList.size() - 1);
            Coordinates firstGeometry = geometryList.get(0);
            Coordinates lastGeometry = geometryList.get(geometryList.size() -1);
            int distanceToFirst = GeoUtils.haversine((float)last.getLat(), (float)last.getLon(), firstGeometry.getLat(), firstGeometry.getLon());
            int distanceToLast = GeoUtils.haversine((float)last.getLat(), (float)last.getLon(), lastGeometry.getLat(), lastGeometry.getLon());

            if(distanceToLast < distanceToFirst){
                //reverse geometryList
                List<Coordinates> tempList = new ArrayList<Coordinates>(geometryList);
                Collections.reverse(tempList);
                geometryList = tempList;
            }


            for(Coordinates coordinates : geometryList){
                coordinateList.add(new Point(coordinates.getLat(), coordinates.getLon()));
            }
            index ++;    
        }
   
        // add coordinates of generated end node
        Long endNodeID = path.getEndVertex();
        lat = mapper.getNodeMap().get(endNodeID).getLat();
        lon = mapper.getNodeMap().get(endNodeID).getLon();
        coordinateList.add(new Point(lat, lon));
        return coordinateList;
    }

}
