from proto.roadnetwork_pb2 import RoadNetwork
import pandas as pd
from google.protobuf.json_format import MessageToDict
import geopandas as gpd
from shapely.geometry import LineString


def parseData(file_path):
    with open(file_path, "rb") as f:
        read_roadnetwork = RoadNetwork()
        read_roadnetwork.ParseFromString(f.read())
        roadnetwork_dict = MessageToDict(read_roadnetwork)
        return roadnetwork_dict


def getNodesDataframe(roadnetwork_dict):
    nodes_df = pd.DataFrame.from_dict(roadnetwork_dict["nodes"].values())
    # positions = nodes_df['position'].apply(lambda x: x.values()).explode().apply(pd.Series)
    df = pd.json_normalize(nodes_df["position"]).set_index(nodes_df.index)
    nodes_df = nodes_df.join(df)
    nodes_df = nodes_df.drop(columns=["position"])
    gdf = gpd.GeoDataFrame(nodes_df, geometry=gpd.points_from_xy(df.lon, df.lat))
    return gdf


def getEdgesDataframe(roadnetwork_dict):
    if "segments" not in roadnetwork_dict.keys():
        return gpd.GeoDataFrame()
    edges_df = pd.DataFrame.from_dict(roadnetwork_dict["segments"].values())
    edges_df["geometry"] = edges_df["geometry"].apply(
        lambda x: [(value["lat"], value["lon"]) for value in x]
    )
    # gdf = gpd.GeoDataFrame(edges_df, geometry=edges_df.geometry)
    return edges_df


def getTurnRestrictions(roadnetwork_dict):
    if "turnRestrictions" not in roadnetwork_dict.keys():
        return gpd.GeoDataFrame()
    turn_restrictions_df = pd.DataFrame.from_dict(roadnetwork_dict["turnRestrictions"])
    return turn_restrictions_df
