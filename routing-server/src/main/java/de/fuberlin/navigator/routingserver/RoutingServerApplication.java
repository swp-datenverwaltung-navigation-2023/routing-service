package de.fuberlin.navigator.routingserver;

import de.fuberlin.navigator.routingserver.utility.Config;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class RoutingServerApplication {

	public static void main(String[] args) {
		Config.load();
		SpringApplication.run(RoutingServerApplication.class, args);
	}

}
