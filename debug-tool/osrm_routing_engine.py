from routing_engine import RoutingEngine
import polyline


class OSRMRoutingEngine(RoutingEngine):
    def __init__(self):
        super().__init__('https://routing.openstreetmap.de/routed-car/route/v1/driving/{},{};{},{}?overview=false&alternatives=true&steps=true')

    def set_starting_point(self, start):
        super().set_starting_point([start[1], start[0]])

    def set_end_point(self, end):
        super().set_end_point([end[1], end[0]])

    def fetch_route(self):
        response = super().fetch_route()
        steps = response['routes'][0]['legs'][0]['steps']

        geometries = map(lambda step: polyline.decode(step['geometry']), steps)

        flat_array = flatten_array(geometries)

        return tuple_array_to_2d_array(flat_array)


def flatten_array(array, flat_array=None):
    if flat_array is None:
        flat_array = []

    for sub_array in array:
        if isinstance(sub_array, list):
            flatten_array(sub_array, flat_array)
            continue

        flat_array.append(sub_array)

    return flat_array


def tuple_array_to_2d_array(array):
    tuple_to_array = lambda tpl: [tpl[0], tpl[1]]

    return list(map(tuple_to_array, array))
