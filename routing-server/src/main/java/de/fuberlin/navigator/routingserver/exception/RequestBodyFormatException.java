package de.fuberlin.navigator.routingserver.exception;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonLocation;

public class RequestBodyFormatException extends JacksonException {

    public RequestBodyFormatException(String msg) {
        super(msg);
    }

    @Override
    public JsonLocation getLocation() {
        return null;
    }

    @Override
    public String getOriginalMessage() {
        return null;
    }

    @Override
    public Object getProcessor() {
        return null;
    }
}
