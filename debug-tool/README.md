## Debug tool

## Initialise environment 

One needs [python3](https://www.python.org/downloads/) and [anaconda](https://www.anaconda.com/products/distribution) before initialising the environment.

In order to initialise the environment, use from the `debug-tool` folder :

```sh
conda env create -f environment.yml
```

The environment is called `swp_debug_tool` by default. You can activate it via the standard command `conda activate swp_debug_tool`.

When adding dependencies, execute : 

```sh
conda env export --from-history | grep -v "^prefix: " > environment.yml
```

The `--from-history` parameter prevents adding the build ID to the dependency, as that is usually OS-specific. Also, it only
adds the dependencies explicitly installed by the user. Their dependencies are then resolved automatically. If you somehow
still have build IDs (i.e. your dependencies have the form `<name>=<version>=<build gibberish`)
, you can use the following regex find-replace in the YAML file, in order to remove the builds manually:
* find: `(- [^=]+=[^=-]+)=[^\n=-]+`
* replace: `$1`

This works on JetBrains IDEs at least.

so that the dependencies are reflected in the state of the project. 

The proto sample should be in the directory `debug-tool/proto/sample` and the tool would take it automatically.

## Tool startup

In order to start the tool, one has to enter the folder `debug-tool` and execute :

```sh
conda activate debug-tool
```

And then open the tool notebook with jupyter lab, e.g. : 

```sh
jupyter lab .
```

## Tool interface 

![picture](photo/1.png)

The tool consists of a map and three buttons on the side. In order to compare routes, one has to have started the routing server and click 2 times on the map. With the first click, the start point of the route is chosen, with the second, the end point of the route. Afterwards, one has to click on the clean/calculate route button.

#### Route calculation

The tool compares the routes computed by our own [routing engine](https://git.imp.fu-berlin.de/swp-datenverwaltung-navigation-2023/routing-service) to the ones, computed by [OSMR](https://project-osrm.org/). One should start the routing service locally with a provied, or a pre-build roadnetwork or start it as a docker container, as described in the repository. 

On first click, the tool selects a start location, on second one an end location and then by clicking on the `create/clear route` button it displays two routes. The blue route is provided by our routing engine and the red one is provided by OSRM. 

![picture](photo/2.png)

The tool main's purpose is to compare the quality of the two routes and to give information about the closeness of the current solution tot the one provided by OSRM.

#### Roadnetwork display 

In many cases during our work, we had to debug problems, which are not directly related to routing, but rather to the underlying roadnetwork we used for routing. Therefore, we thought, it might be quite meaningful to have a roadnetwork view, in order ot debug problems related to it.

Here is a view of it. The nodes and edges are clickable, and on the side widget one can obtain the information about them (IDs, road categories, etc).

#### Loading a new roadnetwork 

In order to load a new roadnetwork, one has to add the produced by `map-data-builder` file in the `proto/sample` folder. This way produced roadnetworks can be tested easily.

![picture](photo/3.png)

The roadnetwork is not displayed on load-up. It can be however displayed, by clicking the `toggle roadnetwork`  button. By clicking it once more, it is hidden again.

#### Limitations

The main limitation of the debug tool, is that if larger roadnetworks are produced, it's rendering might be slow or not possible at all. As the tool renders all the segments and nodes on the map, the larger the network, the more requirements it does have on the system.

Another rather easy to fix limitation is, that currently, we do not integrate the time span within\` it. We didn't have much time after implementing it, in order to properly implement buttons/fields, in order to transmit it to the backend. Therefore currently, we just use hardcoded values, which are basically with the meaning to start one day after today and to finish 2 days after today. These values can be seen in the `own_routing_engine.py` file. As further work this feature can be implemented.