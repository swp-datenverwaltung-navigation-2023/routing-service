from ipyleaflet import Map, basemaps, basemap_to_tiles, GeoData, Marker, AwesomeIcon
from ipywidgets import HTML, Button
from ipyleaflet import WidgetControl, LayerGroup, Polyline
from proto_parser import (
    parseData,
    getNodesDataframe,
    getEdgesDataframe,
    getTurnRestrictions,
)
from osrm_routing_engine import OSRMRoutingEngine
from route_renderer import RouteRenderer


class DebugTool:
    def __init__(self):
        self.__load_data()
        self.route_layer = None
        self.__initialize_map()
        self.__initialize_buttons()
        self.__initialize_routes()

    def __initialize_map(self):
        self.m = Map(
            basemap=basemap_to_tiles(basemaps.OpenStreetMap.Mapnik),
            center=(self.nodes_df.iloc[0].lat, self.nodes_df.iloc[0].lon),
            zoom=17,
        )

        self.m.on_interaction(self.__handle_map_interaction)

        self.__edges_layer = None
        self.__nodes_layer = None

    def __initialize_buttons(self):
        self.__position_for_widgets = 'bottomright'

        self.__initialize_info_widget()
        self.__initialize_route_controls()
        self.__initialize_rendering_toggle()

    def __initialize_route_controls(self):
        route_toggle_button = Button(
            value=False,
            description='Create/clear route',
            disabled=False,
            button_style='',  # 'success', 'info', 'warning', 'danger' or ''
        )
        points_clear_button = Button(
            value=False,
            description='Clear points',
            disabled=False,
            button_style='',  # 'success', 'info', 'warning', 'danger' or ''
        )
        control0 = WidgetControl(widget=route_toggle_button, position=self.__position_for_widgets)
        route_toggle_button.on_click(self.__on_route_control_click)
        control1 = WidgetControl(widget=points_clear_button, position=self.__position_for_widgets)
        points_clear_button.on_click(self.__clear_points)

        self.m.add(control0)
        self.m.add(control1)

    def __clear_points(self, _):
        self.__route_renderer.clear_points()

    def __on_route_control_click(self, _):
        if not self.__route_renderer.has_built_route():
            if self.__route_renderer.is_ready_to_build_route():
                self.__route_renderer.build_route()
        else:
            self.__clear_route()

    def __initialize_info_widget(self):
        self.node_widget = HTML(
            """
            Information Widget
            """
        )
        self.node_widget.layout.margin = "0px 20px 20px 20px"

        control = WidgetControl(widget=self.node_widget, position=self.__position_for_widgets)
        self.m.add(control)

    def __initialize_rendering_toggle(self):
        render_toggle_button = Button(
            value=False,
            description='Toggle network',
            disabled=False,
            button_style='',  # 'success', 'info', 'warning', 'danger' or ''
        )
        control = WidgetControl(widget=render_toggle_button, position=self.__position_for_widgets)
        render_toggle_button.on_click(self.__toggle_network_rendering)

        self.m.add(control)

    def __toggle_network_rendering(self, _):
        if self.__nodes_layer is None:
            self.__initialize_network_layers()
        else:
            self.__clear_network()

    def __clear_network(self):
        self.m.remove(self.__nodes_layer)
        self.m.remove(self.__edges_layer)

        self.__nodes_layer = None
        self.__edges_layer = None

    def __handle_map_interaction(self, **event_data):
        if event_data.get("type", None) == "click":
            coordinates = event_data.get("coordinates")

            if not self.__route_renderer.is_ready_to_build_route():
                self.__route_renderer.add_point(coordinates)

    def __clear_route(self):
        self.__route_renderer.clear_route()

    def __initialize_network_layers(self):
        self.__nodes_layer = GeoData(
            geo_dataframe=self.nodes_df,
            style={
                "color": "black",
                "radius": 8,
                "fillColor": "#3366cc",
                "opacity": 0.5,
                "weight": 1.9,
                "dashArray": "2",
                "fillOpacity": 0.6,
            },
            hover_style={"fillColor": "red", "fillOpacity": 0.2},
            point_style={
                "radius": 5,
                "color": "red",
                "fillOpacity": 0.8,
                "fillColor": "blue",
                "weight": 3,
            },
            name="Intersections",
        )
        self.__nodes_layer.on_click(self.addNodeDescription)
        self.__edges_layer = self.__visualise_segments()
        self.m.add(self.__nodes_layer)
        self.m.add(self.__edges_layer)

    def __visualise_segments(self):
        edges_layer = LayerGroup()
        for _, row in self.edges_df.iterrows():
            layer = Polyline(
                locations=row["geometry"],
                color="green",
                opacity=0.6,
                weight=5,
                fill=False,
            )
            layer.on_click(self.onEdgeClick(row))
            edges_layer.add(layer)
        return edges_layer

    def __initialize_routes(self):
        self.__route_renderer = RouteRenderer()
        layer = self.__route_renderer.get_layer()
        self.m.add(layer)

    def onEdgeClick(self, edge):
        def innerEdgeClick(**_):
            self.node_widget.value = f"""
            EDGE:
            ID: {edge["id"]}, </br>
            OSM ID: {edge["osmId"]}, </br>
            LENGTH: {edge["length"]}, </br>
            START NODE: {edge["startNode"]}, </br>
            END NODE: {edge["endNode"]},
            """

        return innerEdgeClick

    def addNodeDescription(self, **e):
        properties = e["properties"]
        self.node_widget.value = f"""
        NODE: </br>
        ID: {e["feature"]["id"]} </br>
        OSM ID: {properties["osmId"]}, </br>
        LAT/LON: {properties["lat"]}, {properties["lon"]} 
        """

    def __load_data(self):
        roadnetwork = parseData("./proto/sample/roadnetwork_sample.proto")
        self.nodes_df = getNodesDataframe(roadnetwork)
        self.edges_df = getEdgesDataframe(roadnetwork)
        self.turn_restrictions_df = getTurnRestrictions(roadnetwork)
