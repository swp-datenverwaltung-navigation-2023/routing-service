package de.fuberlin.navigator.routingserver.utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import de.fuberlin.navigator.protos.metric_builder.Metrics;
import de.fuberlin.navigator.protos.metric_builder.Metrics.Builder;

public class MetricReader {

    static Metrics metrics = readMetrics();
    public static void main(String[] args) throws IOException {

        // testing
        int dayAfterToday = 0;
        System.out.println(MetricReader.metrics.getMetricsMap().get(dayAfterToday).getMetricsPerDayMap());

    }

    // read from metrics file
    public static Metrics readMetrics(){
        Builder builder = Metrics.newBuilder();
            
        try {
            FileInputStream input = new FileInputStream(Config.METRICS_INPUT_PATH);
            Metrics metrics = builder.mergeFrom(input).build();
            return metrics;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
            

        
    }
 
}

